<?php
/**
 * @file
 * Admin page callbacks for the cart_access module.
 */

function cart_access_admin_form($form, &$form_state) {
  $form['cart_access'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cart Access settings - When the user has not access'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['cart_access']['cart_access_redirect'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect Path'),
    '#default_value' => variable_get('cart_access_redirect', NULL),
    '#description' => t('Path to redirect "Add to Cart" button, if empty returns to product page'),
  );

  $form['cart_access']['cart_access_hide_button'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide Button'),
    '#default_value' => variable_get('cart_access_hide_button', FALSE),
    '#description' => t('Hide "Add to cart" button'),
  );

  $form['cart_access']['cart_access_replace_button_text'] = array(
    '#type' => 'checkbox',
    '#title' => t('Replace Button Text'),
    '#default_value' => variable_get('cart_access_replace_button_text', TRUE),
    '#description' => t('Replace "Add to cart" by "@text" in add to cart button', array('@text' => _cart_access_button_txt())),
    '#states' => array('visible' => array(':input[name="cart_access_hide_button"]' => array('checked' => FALSE))),
  );

  $form = system_settings_form($form);
  return $form;
}
